﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ScreenMgr;
using UnityEngine.Video;

/// <summary>
/// 
/// </summary>
public class DynamicScreen : AnimatorScreen
{

	#region Nested Types

	public enum VideoType { BAM, EVA }

	[System.Serializable]
	public struct VideoSpecs 
	{
		public VideoClip clip;
		public Sprite splashScreen;
		public string proxyTriggerName;
	}

	#endregion

	#region Fields and Properties

	[Header("Specific Specs")]

	[SerializeField]
	MasterSwitch masterSwitch;

	[SerializeField]
	VideoPlayerManager videoPlayerManager;

	[SerializeField]
	VideoType currentVideo;

	[SerializeField]
	VideoSpecs BAM_Specs;

	[SerializeField]
    VideoSpecs EVA_Specs;

	[SerializeField]
	Animator proxyElementsAnim;

    [SerializeField]
    string idle_TriggerName = "Idle_Elements_Trigger";

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

    #endregion

    #region Methods

	public void OnPlayButton_Clicked()
	{
		if(videoPlayerManager.videoPlayer.isPlaying || videoPlayerManager.isPaused)
		{
			videoPlayerManager.TogglePlayPause();
			return;
		}
		switch(currentVideo)
		{
			case VideoType.BAM:
				PlayVideo_BAM();
				break;
			case VideoType.EVA:
                PlayVideo_EVA();
                break;
		}
	}

	public void PlayVideo_BAM()
	{
		currentVideo = VideoType.BAM;

        //proxyElementsAnim.SetTrigger(idle_TriggerName);
        proxyElementsAnim.Play("Anim_[ProxyElements]_Idle");

        proxyElementsAnim.StopPlayback();

        videoPlayerManager.PlayClip(BAM_Specs.clip);
		videoPlayerManager.SetSplashScreenSprite(BAM_Specs.splashScreen);         
		proxyElementsAnim.SetTrigger(BAM_Specs.proxyTriggerName);
	}

	public void PlayVideo_EVA()
    {
		currentVideo = VideoType.EVA;

        //proxyElementsAnim.SetTrigger(idle_TriggerName);
        proxyElementsAnim.Play("Anim_[ProxyElements]_Idle");

        proxyElementsAnim.StopPlayback();

        videoPlayerManager.PlayClip(EVA_Specs.clip);
        videoPlayerManager.SetSplashScreenSprite(EVA_Specs.splashScreen);
		proxyElementsAnim.SetTrigger(EVA_Specs.proxyTriggerName);
    }

	public override void OnAnimationIn()
	{
		base.OnAnimationIn();

        proxyElementsAnim.StopPlayback();

        masterSwitch.OnSwitchToState(0);
        PlayVideo_BAM();
	}

	public override void OnAnimationOut()
	{
		base.OnAnimationOut();

		proxyElementsAnim.StopPlayback();

		masterSwitch.OnSwitchToState(0);
		PlayVideo_BAM();
	}

	#endregion
}
