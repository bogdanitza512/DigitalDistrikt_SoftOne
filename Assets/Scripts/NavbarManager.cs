﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;


/// <summary>
/// 
/// </summary>
public class NavbarManager : SerializedMonoBehaviour
{
    #region Nested Types

    /// <summary>
    /// Button types.
    /// </summary>
	public enum ButtonTypes { Dynamic, Cloud, UX, Collab, Mobile }

	#endregion

	#region Fields and Properties
    
	[SerializeField]
	Animator masterSlideAnim;

	[SerializeField]
	Dictionary<ButtonTypes, string> navbarTriggerMap = new Dictionary<ButtonTypes, string>
	{
		{ButtonTypes.Dynamic, "OnDynamicButton_Trigger"},
		{ButtonTypes.Cloud, "OnCloudButton_Trigger"},
		{ButtonTypes.UX, "OnUXButton_Trigger"},
		{ButtonTypes.Collab, "OnCollabButton_Trigger"},
		{ButtonTypes.Mobile, "OnMobileButton_Trigger"}
	};

	#endregion

	#region Unity Messages

	/// <summary>
	/// Start is called just before any of the Update methods is called the first time.
	/// </summary>
	void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

    #endregion

    #region Methods
    
	public void OnNavbarButtonClicked( int _id )
	{
		//masterSlideAnim.SetTrigger(navbarTriggerMap[(ButtonTypes)_id]);
		print((ButtonTypes)_id);
	}

    #endregion
}
