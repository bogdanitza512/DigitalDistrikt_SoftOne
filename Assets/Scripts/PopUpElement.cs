﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UniExt;
using Sirenix.OdinInspector;

/// <summary>
/// 
/// </summary>
[ExecuteInEditMode]
public class PopUpElement : MonoBehaviour
{

	#region Fields and Properties

	[SerializeField]
	Image activeImage;

	[SerializeField]
	Color activeColor;

	[SerializeField]
	TMP_Text text;

	[SerializeField]
	Image inactiveImage;

	[SerializeField]
	Color inactiveColor;
    
	[SerializeField, Range(0,1)]
	float _alpha;

	float _lastAlpha;

    void RefreshAlpha()
	{
		activeImage.color =
                           activeImage.color.WithAlpha(Mathf.Lerp(0, 1, _alpha));
        inactiveImage.color =
                         inactiveImage.color.WithAlpha(Mathf.Lerp(0, 1, 1 - _alpha));
		text.color = Color.Lerp(inactiveColor, activeColor, _alpha);
	}
     
    public float activeAlpha
	{
		get { return _alpha; }
		set 
		{ 
			_alpha = value;
			RefreshAlpha();
		}
	}

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
		if(System.Math.Abs(_alpha - _lastAlpha) > 0.01)
		{
			RefreshAlpha();
			_lastAlpha = _alpha;
		}
    }

    #endregion

    #region Methods

	

    #endregion
}
