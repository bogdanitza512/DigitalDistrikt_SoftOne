﻿using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class OrbitalMovement : MonoBehaviour 
{
    #region Fields and Properties

    /// <summary>
    /// The player transform component.
    /// </summary>
    [SerializeField]
    [Tooltip("The player transform component.")]
    Transform player;

    /// <summary>
    /// The orbit generator.
    /// </summary>
    [SerializeField]
    [Tooltip("The orbit generator.")]
    OrbitGenerator orbitGenerator;

    /// <summary>
    /// The duration of the cycle.
    /// </summary>
    [SerializeField]
    [Tooltip("The duration of the cycle.")]
    float cycleDuration = 5.0f;

    /// <summary>
    /// The animation curve of a cycle.
    /// </summary>
    [SerializeField]
    [Tooltip("The animation curve of a cycle.")]
    AnimationCurve cycleAnimCurve;

    /// <summary>
    /// The starting orbit point index for the cycle.
    /// </summary>
    [SerializeField]
    [Tooltip("The starting orbit point index for the cycle.")]
    int startingOrbitPointIndex;

    /// <summary>
    /// The orbit points list cached reference.
    /// </summary>
    List<Transform> orbitPoints;

    /// <summary>
    /// The index of the current orbit point.
    /// </summary>
    int currentOrbitPointIndex = 0;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the 
    /// Update methods is called the first time
    /// </summary>
    void Start()
    {
        DOTween.Init();
        if(orbitGenerator != null)
        {
            orbitPoints = orbitGenerator.orbitPoints;
        }
        currentOrbitPointIndex = startingOrbitPointIndex;
        PerformCycle(currentOrbitPointIndex);

    }


    /// <summary>
    /// Update is called every frame, 
    /// if the MonoBehaviour is enabled
    /// </summary>
    void Update()
    {
    }

    #endregion

    #region Methods

    /// <summary>
    /// Does the next cycle.
    /// </summary>
    void DoNextCycle()
    {
        currentOrbitPointIndex = ++currentOrbitPointIndex != orbitPoints.Count ? currentOrbitPointIndex : 0;
        PerformCycle(currentOrbitPointIndex);
    }

    /// <summary>
    /// Starts cycling player transform through orbit points.
    /// </summary>
    void PerformCycle(int index)
    {

        player.DOMove(orbitPoints[index].position, 
                      cycleDuration)
              .SetEase(cycleAnimCurve)
              .OnComplete(DoNextCycle);
        
        player.DORotate(orbitPoints[index].eulerAngles, 
                        cycleDuration)
              .SetEase(cycleAnimCurve);
        
    }



    #endregion
}
