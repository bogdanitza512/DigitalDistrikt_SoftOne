﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine.UI;
using DG.Tweening;
using UniExt;

/// <summary>
/// 
/// </summary>
public class VideoPlayerManager_UX : MonoBehaviour
{

	#region Fields and Properties

	[SerializeField]
	VideoPlayer videoPlayer;

	[SerializeField]
	VideoClip UX_Video;

	[SerializeField]
	Animator videoProxyElementsAnim;

	[SerializeField]
	string UX_ElementsTriggerName;
   
	[SerializeField]
	RawImage screen;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
		videoPlayer.prepareCompleted
				   += (source) =>
		{
			screen.DOFade(1, .5f);
			// playButton.image.color = playButton.image.color.WithAlpha(0);
		};
		videoPlayer.loopPointReached
				   += (source) =>
		{
			screen.DOFade(0, 2.5f);
			// playButton.image.DOFade(1, 2.5f);
		};

		Play_UXVideo();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

	#endregion

	#region Methods

	public void StopVideoPlayer()
	{
		if(videoPlayer.isPlaying)
		    videoPlayer.Stop();
	}

	public void Play_UXVideo()
	{
		videoPlayer.clip = UX_Video;
		videoPlayer.Play();
		videoProxyElementsAnim.SetTrigger(UX_ElementsTriggerName);
	}

    #endregion
}
