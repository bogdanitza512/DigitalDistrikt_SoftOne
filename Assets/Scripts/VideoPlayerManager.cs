﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
[RequireComponent(typeof(VideoPlayer),typeof(RawImage))]
public class VideoPlayerManager : MonoBehaviour
{

	#region Fields and Properties

	public VideoPlayer videoPlayer;

	public RawImage display;

	public Image splashScreen;

	public Button playButton;

	public bool isPaused = false;

	[SerializeField]
	float fadeDuration = 2.5f;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
	{
		
		EnsureRequiredRefs();

		HookUpBehaviourToVideoPlayerEvents();

	}

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	void Update()
    {
       
    }

    #endregion

    #region Methods

	private void EnsureRequiredRefs()
    {
        if (videoPlayer == null)
        {
            videoPlayer = GetComponent<VideoPlayer>();
        }

        if (display == null)
        {
            display = GetComponent<RawImage>();
        }
    }

	private void HookUpBehaviourToVideoPlayerEvents()
    {
        videoPlayer.started
                   += (source) =>
		{
			display.DOFade(1, fadeDuration / 2);
			playButton.image.DOFade(0, fadeDuration / 3);
			splashScreen.DOFade(0, fadeDuration / 4);
		};

        videoPlayer.loopPointReached
                   += (source) =>
		{
			display.DOFade(0, fadeDuration);
			playButton.image.DOFade(1, fadeDuration / 3);
			splashScreen.DOFade(1, fadeDuration / 4);
		};
    }
    
	public void PlayClip(VideoClip _clip)
	{
		videoPlayer.clip = _clip;
		videoPlayer.Play();
	}

	public void SetSplashScreenSprite(Sprite _sprite)
	{
		splashScreen.sprite = _sprite;
	}
    
    public void TogglePlayPause()
	{
		if(isPaused)
		{
			videoPlayer.Play();
			isPaused = false;
			playButton.image.DOFade(0, fadeDuration / 4);
		}
		else
		{
			videoPlayer.Pause();
			isPaused = true;
			playButton.image.DOFade(1, fadeDuration / 2);
		}
	}


    
    #endregion
}
