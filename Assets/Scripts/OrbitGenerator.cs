﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniExt;
using Sirenix.OdinInspector;

public class OrbitGenerator : MonoBehaviour
{

    #region Fields and Properties

    /// <summary>
    /// The number of points.
    /// </summary>
    [SerializeField]
    [Tooltip("Number of points on radius to place prefabs.")]
    int numberOfPoints = 5;

    /// <summary>
    /// The point of interest.
    /// </summary>
    [Tooltip("Transform that generated points orbit around.")]
    public Transform pointOfInterest;

    /// <summary>
    /// The point prefab.
    /// </summary>
    [SerializeField]
    [Tooltip("Generic prefab to place on each point.")]
    GameObject pointPrefab;

    /// <summary>
    /// The radius for x axis.
    /// </summary>
    [SerializeField]
    [Tooltip("Radii x axis.")]
    float radiusX = 5.0f;

    /// <summary>
    /// The radius for y axis.
    /// </summary>
    [SerializeField]
    [Tooltip("Radii for y axis.")]
    float radiusY = 5.0f;

    /// <summary>
    /// The plain in which the points are generated.
    /// </summary>
    enum GenPlane { XY, XZ, YZ }
    [SerializeField]
    [Tooltip("Plane in which to generate.")]
    GenPlane generationPlane = GenPlane.XZ;

    /// <summary>
    /// The should look at point of interest.
    /// </summary>
    [SerializeField]
    [Tooltip("Should generated points look at the point of interest?")]
    bool shouldLookAtPointOfInterest = true;

    /// <summary>
    /// The generic name for generated points.
    /// </summary>
    [SerializeField]
    [Tooltip("Generic name for the generated GameObjects.")]
    string genericName = "Point";

    /// <summary>
    /// Generate orbit points button.
    /// </summary>
    [Button("Generate Orbit Points", ButtonSizes.Medium)]
    void OnClickGenerateOrbitPointsBtn()
    {
        if (!orbitPoints.IsEmpty())
        {
            foreach (var point in orbitPoints)
            {
                if(point != null)
                    DestroyImmediate(point.gameObject);
            }
            orbitPoints.Clear();
        }
        GenerateOrbitPoints();
    }

    /// <summary>
    /// The orbit points.
    /// </summary>
    public List<Transform> orbitPoints = new List<Transform>();

    #endregion

    #region Unity Messages

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        if(!orbitPoints.IsEmpty())
        {
            for (int i = 0; i < orbitPoints.Count; i++)
            {
                if(i+1 != orbitPoints.Count)
                    Gizmos.DrawLine(orbitPoints[i].position,
                                    orbitPoints[i + 1].position);
                else
                    Gizmos.DrawLine(orbitPoints[i].position,
                                    orbitPoints[0].position);
            }
        }

    }

    #endregion

    #region Methods

    void GenerateOrbitPoints()
    {
        for (int i = 0; i < numberOfPoints; i++)
        {
            //multiply 'i' by '1.0f' to ensure the result is a fraction
            var pointNum = (i * 1.0f) / numberOfPoints;

            //angle along the unit circle for placing points
            var angle = pointNum * Mathf.PI * 2;


            var x = Mathf.Sin(angle) * radiusX;
            var y = Mathf.Cos(angle) * radiusY;

            var newOrbitPoint = Instantiate(pointPrefab, pointOfInterest);

            //position for the point prefab
            switch (generationPlane)
            {
                case GenPlane.XY:
                    newOrbitPoint.transform.localPosition = 
                        pointOfInterest.position.With(x: x, y: y);
                    break;

                case GenPlane.XZ:
                    newOrbitPoint.transform.localPosition = 
                        pointOfInterest.position.With(x: x, z: y);
                    break;

                case GenPlane.YZ:
                    newOrbitPoint.transform.localPosition = 
                        pointOfInterest.position.With(y: x, z: y);
                    break;

                default:
                    newOrbitPoint.transform.localPosition = 
                        pointOfInterest.position;
                    break;
            }

            if(shouldLookAtPointOfInterest)
                newOrbitPoint.transform.LookAt(pointOfInterest);
            newOrbitPoint.name = "{0} #{1}".FormatWith(genericName, i + 1);
            orbitPoints.Add(newOrbitPoint.transform);
        }
    }

    #endregion

}
