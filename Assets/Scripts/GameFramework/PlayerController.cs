﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UniExt;

/// <summary>
/// Player Controller Class
/// </summary>
public class PlayerController : SerializedMonoSingleton<PlayerController>
{


    #region Fields and Properties

    [SerializeField]
    private KeyCode restartKey = KeyCode.R;

    [SerializeField]
    private KeyCode exitKey = KeyCode.Q;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if(Input.GetKeyDown(restartKey))
        {
            GameMode.Instance.RestartScene();
        }

        if (Input.GetKeyDown(exitKey))
        {
            GameMode.Instance.QuitGame();
        }
    }




#endregion

    #region Methods

	

    #endregion
}
