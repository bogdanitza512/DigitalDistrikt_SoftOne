﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UniExt;

/// <summary>
/// 
/// </summary>
public class GameMode : SerializedMonoSingleton<GameMode>
{

    #region Fields and Properties

    /// <summary>
    /// The target resolution.
    /// </summary>
    [SerializeField]
    [Tooltip("The target resolution.")]
    Vector2Int targetResolution = new Vector2Int(1080, 1920);

    /// <summary>
    /// The target screen mode.
    /// </summary>
    [SerializeField]
    [Tooltip("The target screen mode.")]
    FullScreenMode targetScreenMode = FullScreenMode.FullScreenWindow;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        Screen.SetResolution(targetResolution.x,
                             targetResolution.y,
                             targetScreenMode);
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
    }

    #endregion

    #region Methods

    /// <summary>
    /// Restarts the scene.
    /// </summary>
	public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Quits the game.
    /// </summary>
    public void QuitGame()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        print("Unity.Editor::Playmode stopped by {0}".FormatWith(this.name));
        #else
        Application.Quit();
        #endif
    }

    #endregion
}
