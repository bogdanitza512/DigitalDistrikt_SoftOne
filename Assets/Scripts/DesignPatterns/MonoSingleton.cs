﻿using UniExt;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
{

    #region Fields and Properties

    protected static T _instance;

    private static object _lock = new object();

    private static bool applicationIsQuitting = false;

    public static bool printDebugMsg = false;

    public static bool deleteDuplicates = true;

    public static T Instance
    {
        get
        {
            if (applicationIsQuitting)
            {
                if (printDebugMsg)
                    print("[MonoSingleton]: Instance '" + typeof(T) +
                        "' already destroyed on application quit." +
                        " Won't create again - returning null.");
                return null;
            }

            lock (_lock)
            {
                if (_instance == null)
                {

                    SearchForInstances();

                    if (_instance == null)
                    {
                        InstantiateSingleton();
                    }
                    else
                    {
                        if (printDebugMsg)
                            print("[MonoSingleton] Using instance already created: " +
                                _instance.gameObject.name);
                    }
                }

                return _instance;
            }
        }
    }

    #endregion

    #region Unity Messages

    /// <summary>
    /// When Unity quits, it destroys objects in a random order.
    /// In principle, a Singleton is only destroyed when application quits.
    /// If any script calls Instance after it have been destroyed, 
    /// it will create a buggy ghost object that will stay on the Editor scene
    /// even after stopping playing the Application. Really bad!
    /// So, this was made to be sure we're not creating that buggy ghost object.
    /// </summary>
    public void OnDestroy()
    {
        if (printDebugMsg)
            print("[MonoSingleton]: {0} is being destroyed.".FormatWith(typeof(T).Name));
        applicationIsQuitting = true;
    }

    #endregion

    #region Methods

    private static void SearchForInstances()
    {
        var singletonsFound = FindObjectsOfType(typeof(T));
        _instance = singletonsFound[0] as T;
        if (singletonsFound.Length > 1)
        {
            if (printDebugMsg)
                print("[MonoSingleton]: Something went really wrong " +
                      " - there should never be more than 1 singleton!" +
                      " Reopening the scene might fix it.");
            if (deleteDuplicates)
            {
                foreach (T obj in singletonsFound)
                {
                    if (obj != _instance)
                        Destroy(obj);

                }
            }
        }
    }

    private static void InstantiateSingleton()
    {
        GameObject singleton = new GameObject();
        _instance = singleton.AddComponent<T>();
        singleton.name = "[" + typeof(T) + "]";

        DontDestroyOnLoad(singleton);

        if (printDebugMsg)
            print("[MonoSingleton] An instance of " + typeof(T) +
                " is needed in the scene, so '" + singleton +
                "' was created with DontDestroyOnLoad.");
    }

    #endregion

}
